# gptc_scratch

gptc_scratch uses the same algorithm as GPTC, in a Scratch project. However, it
only supports two categories, and they are numbered (1 and 2) rather than
named.

Models are formatted differently than in the Python version of GPTC. A model
consists of two files, each containing the text for a category. gptc_scratch
does not have a tokenizer, so each line must consist of one lowercase word with
no other characters. To load the model, right-click "Category 1 Words" in the
project, click "import," then choose one of the files. Do the same thing again
with "Category 2 Words" and the other file.

The text to classify must be stored in the same format as the model files.
Follow the same procedure that you used for loading the model files, but with
"Words in text to classify" and the file containing the text to classify. Then,
click "Compile." If the button changes to a lighter color, wait for it to
change back. Next, click "classify." This should update the "Cat1" and "Cat2"
variables with the confidence valued for the given text and categories.
