import gptc.tokenizer

words = []

while True:
    try:
        words += gptc.tokenizer.tokenize(input())
    except EOFError:
        break

print("\n".join(words))
